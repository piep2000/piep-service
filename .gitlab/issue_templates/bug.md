/label ~"type:bug"
### Geconstateerd door
<!-- Vul hier je naam in zodat de developers weten bij wie ze moeten zijn voor meer informatie -->
Bijvoorbeeld: Mark Veenstra

### Mobilea core versie
<!-- Versie kun je vinden in de cockpit -->
Bijvoorbeeld: 3.4.0

### Huidige gedrag
<!-- Wat gebeurd er op dit moment? -->

### Verwachte gedrag
<!-- Wat had je verwacht dat er ging gebeuren? -->

### Stappen om te reproduceren
<!--
Probeer zoveel mogelijk de juiste stappen te noteren. Denk hierbij aan onder welke user ben je ingelogd bijvoorbeeld en wat kunnen wij doen om zelfde situatie na te bootsen.
-->

### Log informatie

```
Voeg relevante log informatie hier toe
```

### Relevante code

```
Voeg relevante code hier toe
```

### Overige informatie
<!-- Nog tips? Links naar bijvoorbeeld andere apps, afbeeldingen of logfiles of iets dergelijks of andere hulpmiddelen voor oplossen of verduidelijken van dit issue -->

### Volgende mensen up2date houden
@mobilea-gitlab

<!-- Vergeet ook niet de juiste labels toe te voegen, zoals als het de agenda betreft of beeldbellen de juiste label voor functionaliteit -->
