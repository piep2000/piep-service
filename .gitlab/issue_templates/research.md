/label ~"type:research"
### Onderzoek aangevraagd door
<!-- Wie heeft dit onderzoek aangevraagd -->

### Achtergrond
<!-- Probleem dat eventueel opgelost moet worden, waar kan aangevuld met use-cases etc. -->

### Vragen
<!-- Welke vragen dienen er beantwoordt te worden? -->
* Vraag 1
* Vraag 2

### Doel
<!-- Wat is het doel dat bereikt moet worden? Waaraan meten we succes? Moet dit resulteren in work-items? -->

### Links/referenties
<!-- Zijn er links of referenties voor dit onderzoek -->
* [Link 1](https//google.nl)
* [Link 2](https//mobilea.nl)

### Volgende mensen up2date houden
@mobilea-gitlab

<!-- Vergeet ook niet de juiste labels toe te voegen, zoals als het de agenda betreft of beeldbellen de juiste label voor functionaliteit -->