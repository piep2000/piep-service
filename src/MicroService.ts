import {ResourceConflictError} from '@mbl/base';
import {Logger}                from '@mbl/logger';
import {Message}               from './models/Message';
import {settings}              from './settings/Settings';

/**
 * Configuring the Logger as soon as possible so also dependant modules Log with the correct settings
 */
Logger.configure(settings.logging);

import {ApplicationBase}      from '@mbl/base';
import {Injectable, Injector} from '@mbl/di';
import {Http, Route}          from '@mbl/http';
import FireStore              from './storage/FireStore';
import RssStorage             from './storage/RssStorage';

@Route.prefix('/')
@Injectable({singleton: true})
export class MicroService extends ApplicationBase {

    /**
     * Configure the `http` module before it gets injected for the first time
     *
     * @param {Http} http
     */
    @Injector.configure(Http)
    public httpConfigure(http: Http) {
        this.log.info('Configuring HTTP');
        http.configure(settings.http);
    }

    /**
     * Configure the `MongoMobilea` module before it gets injected for the first time
     *
     * @param {MongoMobilea} mongo
     */
    @Injector.configure(FireStore)
    public fireStoreConfigure(fireStore: FireStore) {
        this.log.info('Configuring FireStore');
        fireStore.configure(settings.fireStore);
    }

    @Injector.configure(RssStorage)
    public rssConfigure(rss: RssStorage) {
        this.log.info('Configuring RSS');
        rss.configure(settings.rss);

        rss.on('message', async (messageObj: any) => {
            const logger = this.log.create(messageObj.guid).info('Received article, trying to save it').createTimedLogger();
            try {
                const message = await Message.get({guid: messageObj.guid});
                if (message) {
                    logger.errorOut('Message already exists!');
                } else {
                    try {
                        await Message.create({
                            date: messageObj.date,
                            description: messageObj.description,
                            guid: Number(messageObj.guid),
                            summary: messageObj.summary,
                            title: messageObj.title
                        });
                        logger.info('Article saved');
                    } catch (e) {
                        if (e instanceof ResourceConflictError) {
                            logger.error('Conflict found');
                        } else {
                            logger.errorOut(e);
                        }
                    }
                }
            } catch (e) {
                this.log.error(e);
            }
        });
    }

    /**
     * Bootstrap and start-up the Micro Service. Gets called by the abstract {@link ApplicationBase} classes `run` function.
     *
     * @return {Promise<void>}
     */
    public async start(): Promise<void> {
        MicroService.configure(settings);

        Injector.load('.', __filename);

        const fireStore = Injector.get(FireStore);
        await fireStore.ensureConnection();

        const rss = Injector.get(RssStorage);
        await rss.ensureConnection();

        const http = Injector.get(Http);
        await http.listen();
    }

    /**
     * Closes all connections.
     *
     * @return {Promise<void>}
     */
    public async stop(): Promise<void> {
        const http = Injector.get(Http);
        await http.close();

        const fireStore = Injector.get(FireStore);
        await fireStore.close();

        return super.stop();
    }
}
