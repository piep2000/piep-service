import {ResourceConflictError} from '@mbl/base';
import {Injector}              from '@mbl/di';
import {Logger}                from '@mbl/logger';
import {ObjectID}              from 'bson';
import {forEach, has, isArray} from 'lodash';
import {Counter}               from '../Counter';
import {IMessageCreate}        from '../schema/interfaces/IMessageCreate';
import {IMessageDocument}      from '../schema/interfaces/IMessageDocument';
import {IMessageFind}          from '../schema/interfaces/IMessageFind';
import {IParamsId}             from '../schema/interfaces/IParamsId';
import FireStore               from '../storage/FireStore';
import Query = FirebaseFirestore.Query;
import QueryDocumentSnapshot = FirebaseFirestore.QueryDocumentSnapshot;

/**
 * This Class provides functions in a static way to gain access to the data this class will reflect in an
 * instantiated state.
 *
 * This is merely an example and the provided functions are there only to provide an example on how you could
 * implement such functions.
 */
export class Message implements IMessageDocument {

    public static counter: Counter = new Counter('messages');

    public static log: Logger = Logger.create('Message');

    /**
     * Provide at least 1 parameter you want to find the example data by
     *
     * @param {IParamsId} params
     * @return {Promise<Example>}
     */
    public static async get(params: IMessageFind): Promise<Message> {
        const fireStore = Injector.get(FireStore);
        const collectionRef = await fireStore.collection(Message.collection);
        let queryRef = <Query> collectionRef;
        forEach(Object.keys(Message.prototype), value => {
            queryRef = queryRef.select(value);
        });
        forEach(params, (value, key) => {
            queryRef = queryRef.where(key, '==', value);
        });
        const data = await queryRef.get();
        if (data.size === 1) {
            return new Message({
                id: data.docs[0].id,
                ...<IMessageDocument>data.docs[0].data()
            });
        }
        return null;
    }

    /**
     * Return an array of found examples. With optional pagination.
     *
     * @param params
     * @param {{perPage: number; page: number}} query
     * @returns {Promise<{count: number; data: any[]}>}
     */
    public static async getList(params: any, query: { perPage: number, page: number }): Promise<{ count: number; data: Message[] }> {
        const results = [];
        const fireStore = Injector.get(FireStore);

        const collectionRef = await fireStore.collection(Message.collection);
        let queryRef = <Query> collectionRef;

        if (!this.docCache[query.perPage]) {
            this.docCache[query.perPage] = {};
        }
        const docCache = this.docCache[query.perPage][query.page] || undefined;

        if (docCache) {
            this.log.warn(`Cache found, using it to only read a total of ${query.perPage} documents in one go!`);
            queryRef = queryRef.limit(query.perPage);
            queryRef = queryRef.startAfter(docCache);
        } else {
            this.log.warn(`No cache found, reading a total of ${((query.page - 1) * query.perPage) + query.perPage} documents in one go!`);
            queryRef = queryRef.limit(((query.page - 1) * query.perPage) + query.perPage);
            // queryRef = queryRef.offset((query.page - 1) * query.perPage);
        }

        const snapshot = await queryRef.get();
        let docs = snapshot.docs;

        // If we haven't cached it yet we gonna go all the way back and make caches for each page with the perPage.
        if (!docCache) {
            for (let i = query.page; i > 0; i--) {
                this.docCache[query.perPage][i] = snapshot.docs[((i * query.perPage) - query.perPage) - 1];
            }
            docs = docs.slice(((query.page - 1) * query.perPage), ((query.page - 1) * query.perPage) + query.perPage);
        }

        for (const doc of docs) {
            results.push(new Message({
                id: doc.id,
                ...<IMessageDocument>doc.data()
            }));
        }

        return {
            count: this.counter.estimatedTotal === 0 ? await this.counter.total() : this.counter.estimatedTotal,
            data: results
        };
    }

    /**
     * Return an array of found Messages.
     *
     * @param {number} amount
     * @returns {Promise<Message[]>}
     */
    public static async getLast(amount: number): Promise<Message[]> {
        if (amount > 100) {
            this.log.errorOut(`Given amount '${amount}' is above the limit of 100`);
        }
        if (this.lastHunderdMessages.length >= 100) {
            return this.lastHunderdMessages.slice(0, amount);
        }
        this.log.warn('Reading the last 100 messages from Firestore');

        const results = [];
        const fireStore = Injector.get(FireStore);

        const collectionRef = await fireStore.collection(Message.collection);
        let queryRef = collectionRef.orderBy('date', 'desc');

        queryRef = queryRef.limit(100);

        const snapshot = await queryRef.get();

        for (const doc of snapshot.docs) {
            results.push(new Message({
                id: doc.id,
                ...<IMessageDocument>doc.data()
            }));
        }
        this.lastHunderdMessages = results;

        return results.slice(0, amount);
    }

    /**
     * Stores a new Example document in the Example collection
     *
     * @param {object} data
     *
     * @return {Promise<Example>}
     * @throws {ResourceConflictError}  - When the data for a specific id already existed.
     * @throws {Error}                  - When data could not be stored.
     */
    public static async create(data: IMessageCreate): Promise<Message> {
        const fireStore = Injector.get(FireStore);
        const example = await this.get(data);
        if (example) {
            throw new ResourceConflictError({resourceId: example.id});
        }

        const collectionRef = await fireStore.collection(Message.collection);
        const ref = await collectionRef.add({
            ...data
        });
        if (ref) {
            await this.counter.increment(1);
            const message = new Message({
                id: ref.id,
                ...data
            });
            this.lastHunderdMessages.unshift(message);
            this.lastHunderdMessages.pop();
            return message;
        }
        /* istanbul ignore next */
        throw new Error('Error creating new Example');
    }

    protected static docCache: {
        [key: number]: {
            [key: number]: QueryDocumentSnapshot
        }
    } = {};

    protected static collection: string = 'messages';

    protected static lastHunderdMessages: Message[] = [];

    protected static convertParams(params: any) {
        if (has(params, '_id')) {
            params._id = new ObjectID(params._id);
        }
        return params;
    }

    public id: string;
    public guid: number;
    public date: Date;
    public description: string;
    public summary: string;
    public title: string;

    constructor(data: IMessageDocument) {
        this.id = data.id;
        this.date = new Date(data.date._seconds * 1000);
        this.description = data.description;
        this.summary = data.summary;
        this.title = data.title;
        this.guid = data.guid;
    }
}
