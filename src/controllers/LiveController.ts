import {ResourceConflictError, Utils}                             from '@mbl/base';
import {Injectable}                                               from '@mbl/di';
import {HttpControllerBase, IRequest, IResponse, Route, Validate} from '@mbl/http';
import Paginated                                                  from '@mbl/http/src/Paginated';
import {Message}                                                  from '../models/Message';

@Injectable({singleton: true})
@Route.prefix('/live')
export class LiveController extends HttpControllerBase {
    @Route.get('/')
    public async list(req: IRequest, res: IResponse) {
        const messages = await Message.getLast(100);
        return this.sendJsonOr404(res, messages);
    }
}
