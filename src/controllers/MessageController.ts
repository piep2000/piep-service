import {ResourceConflictError, Utils}                             from '@mbl/base';
import {Injectable}                                               from '@mbl/di';
import {HttpControllerBase, IRequest, IResponse, Route, Validate} from '@mbl/http';
import Paginated                                                  from '@mbl/http/src/Paginated';
import {Message}                                                  from '../models/Message';

@Injectable({singleton: true})
@Route.prefix('/messages')
export class MessageController extends HttpControllerBase {
    /**
     * Returns a list of Messages.
     *
     * @param {IRequest} req
     * @param {IResponse} res
     * @return {Promise<IResponse>}
     */
    @Paginated()
    @Route.get('/')
    public async list(req: IRequest, res: IResponse) {
        const {count, data} = await Message.getList({}, {perPage: req.query.perPage, page: req.query.page});
        res.totalCount = count;
        return this.sendJsonOr404(res, data);
    }

    /**
     * Returns the requested Example.
     *
     * @param {IRequest} req
     * @param {IResponse} res
     * @return {Promise<IResponse>}
     */
    @Route.get('/:_id')
    @Validate.json({
        request: {
            params: './src/schema/params.id.json'
        },
        response: './src/schema/message.document.json'
    })
    public async get(req: IRequest, res: IResponse): Promise<IResponse> {
        try {
            return this.sendJsonOr404(res, await Message.get(req.params));
        } catch (err) {
            this.log.error(err);
            return res.sendStatus(500);
        }
    }

    /**
     * Method for creating a new Example document in the example collection.
     *
     * When the resource to be created already exists a PUT/PATCH request is necessary and a
     * `Location` header is returned to the client containing the location of the resource in
     * combination with a 409 statusCode.
     *
     * @param {IRequest} req
     * @param {IResponse} res
     * @return {Promise<IResponse>}
     */
    @Route.post('/')
    @Validate.json({
        request: {
            body: './src/schema/message.create.json'
        },
        response: './src/schema/message.document.json'
    })
    public async create(req: IRequest, res: IResponse) {
        try {
            const example = await Message.create(req.body);
            return res.status(201).json(example);
        } catch (err) {
            if (err instanceof ResourceConflictError) {
                this.log.warn(err);
                res.header('Location', `${Utils.joinUri(req.originalUrl, err.resourceId)}`);
                return res.sendStatus(409);
            }
            this.log.error(err);
            return res.sendStatus(500);
        }
    }
}
