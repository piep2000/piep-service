import {FieldValue} from '@google-cloud/firestore';
import {ClassBase}  from '@mbl/base';
import {Injector}   from '@mbl/di';
import Bottleneck from 'bottleneck';
import FireStore    from './storage/FireStore';

export class Counter extends ClassBase {
    protected _name: string;
    protected _shards: number;
    protected _total: number = 0;

    protected currentShard: number = 0;

    protected limiter: Bottleneck;

    public get name(): string {
        return this._name;
    }

    public get shards(): number {
        return this._shards;
    }

    public get estimatedTotal(): number {
        return this._total;
    }

    public constructor(name: string, shards: number = 10) {
        super();
        this._name = name;
        this._shards = shards;
        this.limiter = new Bottleneck({
            maxConcurrent: Math.floor(this.shards / 2),
            minTime: 1000 / this.shards
        });
    }

    public async increment(value: number) {
        if (Math.sign(value) > 0) {
            return await this.limiter.schedule(() => this.update(value));
        }
        throw new Error(`Given value '${value}' is not positive, can't be used to increment`);
    }

    public async decrement(value: number) {
        if (Math.sign(value) < 0) {
            return await this.update(value);
        }
        throw new Error(`Given value '${value}' is not negative, can't be used to decrement`);
    }

    public async total() {
        const fireStore = Injector.get(FireStore);
        const ref = fireStore.db.collection('counters').doc(this.name);
        const shards = await ref.collection('shards').get();
        let totalCount = 0;
        for (const doc of shards.docs) {
            totalCount += doc.data().count;
        }

        this._total = totalCount + 1;
        return totalCount;
    }

    protected async update(value: number) {
        if(this._total === 0) {
            await this.total();
        }
        const fireStore = Injector.get(FireStore);
        const ref = fireStore.db.collection('counters').doc(this.name);

        const batch = fireStore.db.batch();
        batch.set(ref, {numShards: this.shards});
        for (let i = 0; i < this.shards; i++) {
            const shardRef = ref.collection('shards').doc(i.toString());
            if (!(await shardRef.get()).exists) {
                batch.set(shardRef, {count: 0});
            }
        }
        await batch.commit();

        if (this.currentShard + 1 >= this.shards) {
            this.currentShard = 0;
        }

        this.currentShard++;
        const shardRef = ref.collection('shards').doc(this.currentShard.toString());
        await shardRef.update('count', FieldValue.increment(value));
        this._total += value;
    }
}
