import AStorageProviderBase from '@mbl/base/src/AStorageProviderBase';
import {Injectable}         from '@mbl/di';
import Bottleneck           from 'bottleneck';
export let Watcher: any;
/* tslint:disable */
Watcher = require('rss-watcher');

export interface IRssSettings {
    url?: string;
    interval: number;
    connectOptions: {
        ensureConnectionTimeout: number;
        reconnectInterval: number;
        reconnectTries: number;
    };
}

@Injectable({singleton: true})
export default class RssStorage extends AStorageProviderBase {
    protected _watcher: any;

    public get isConnected(): boolean {
        return this.client ? true : false;
    }

    protected settings: IRssSettings = {
        connectOptions: {
            ensureConnectionTimeout: 60000,
            reconnectInterval: 1000,
            reconnectTries: 10
        },
        interval: 60
    };

    /**
     * Return the internal MongoClient.
     *
     * @returns {MongoClient}
     */
    public get client() {
        return this._watcher;
    }

    protected limiter: Bottleneck;

    private _onArticle = this.onArticle.bind(this);
    private _onError = this.onError.bind(this);

    public constructor() {
        super();
        this.limiter = new Bottleneck({
            maxConcurrent: 1,
            minTime: 1000,
        });
    }

    public on(event: 'message', cb: (article: any) => void): this {
        return super.on(event, cb);
    }

    public async connect() {
        const timedLogger = this.log.createTimedLogger().debug('Trying to connect to RSS feed');
        let attempt = 0;
        return new Promise((resolve, reject) => {
            const onSuccess = () => {
                timedLogger.debug('Connection to RSS feed established!');
                this.client.on('new article', this._onArticle);
                this.client.on('error', this._onError);
                resolve(this.client);
            };
            const onError = (err: Error) => {
                if (++attempt > this.settings.connectOptions.reconnectTries || !~err.message.indexOf('ENOTFOUND')) {
                    this.log.error(err);
                    return reject(err);
                }
                this.log.info(`Connect attempt(${attempt}) failed with error: ${err.message}`);
                setTimeout(() => {
                    this._connect().then(onSuccess).catch(onError);
                }, this.settings.connectOptions.reconnectInterval);
            };
            this._connect()
                .then(onSuccess)
                .catch(onError);
        });
    }

    public async close() {
        if (this.client) {
            this.client.off('new article', this._onArticle);
            this.client.off('error', this._onError);
            this.client.stop();
        }
        this._watcher = undefined;
    }

    /**
     * The internal method to setup the real connection.
     *
     * @returns {Promise<void>}
     * @private
     */
    protected _connect() {
        return new Promise((resolve, reject) => {
            if (!this.settings.url) {
                return this.log.critical('RSS_URL not provided!');
            }
            this._watcher =  new Watcher(this.settings.url);
            this._watcher.set({interval: this.settings.interval});
            this._watcher.run(async (err: Error, articles: any[]) => {
                if(err) { return reject(err); }
                for(const article of articles) {
                    // this.onArticle(article);
                }
                resolve();
            });
        });
    }

    protected onArticle(article: any) {
        this.limiter.schedule(() => this.emit.apply(this, (<any[]>['message']).concat([article])));
    }

    protected onError(err: Error) {
        this.log.error(err);
        this.emit.apply(this, (<any[]>['error']).concat([err]));
    }
}
