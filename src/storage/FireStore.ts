import {Firestore} from '@google-cloud/firestore';
import AStorageProviderBase    from '@mbl/base/src/AStorageProviderBase';
import {Injectable, Injector}  from '@mbl/di';

export interface IFireStoreSettings {
    projectId?: string;
    credentials?: {[key: string]: string};
    connectOptions: {
        ensureConnectionTimeout: number;
        reconnectInterval: number;
        reconnectTries: number;
    };
}

@Injectable({singleton: true})
export default class FireStore extends AStorageProviderBase {
    public db: Firestore;

    public get isConnected(): boolean {
        return this.db ? true : false;
    }

    protected settings: IFireStoreSettings = {
        connectOptions: {
            ensureConnectionTimeout: 60000,
            reconnectInterval: 1000,
            reconnectTries: 10
        }
    };

    public async collection(collectionPath: string) {
        return await this.db.collection(collectionPath);
    }

    public async connect() {
        const timedLogger = this.log.createTimedLogger().debug('Trying to connect to Firestore');
        let attempt = 0;
        return new Promise((resolve, reject) => {
            const onSuccess = () => {
                timedLogger.debug('Connection to Firestore established!');
                resolve(this.db);
            };
            const onError = (err: Error) => {
                if (++attempt > this.settings.connectOptions.reconnectTries || !~err.message.indexOf('UNAVAILABLE')) {
                    this.log.error(err);
                    return reject(err);
                }
                this.log.info(`Connect attempt(${attempt}) failed with error: ${err.message}`);
                setTimeout(() => {
                    this._connect().then(onSuccess).catch(onError);
                }, this.settings.connectOptions.reconnectInterval);
            };
            this._connect()
                .then(onSuccess)
                .catch(onError);
        });
    }

    public async close() {
        if (this.db) {
            //
        }
        this.db = undefined;
    }

    /**
     * The internal method to setup the real connection.
     *
     * @returns {Promise<void>}
     * @private
     */
    protected _connect() {
        return new Promise(async (resolve, reject) => {
            if (!this.settings.projectId) {
                return this.log.critical('fireStore_projectId not provided!');
            }
            if (!this.settings.credentials) {
                return this.log.critical('fireStore_keyFilename not provided!');
            }
            this.db = new Firestore({
                credentials: this.settings.credentials,
                projectId: this.settings.projectId
            });

            const collectionRef = this.db.collection('connect');
            try {
                const docRef = await collectionRef.doc();
                await docRef.delete();
                resolve(this.db);
            } catch(e) {
                reject(e);
            }
        });
    }
}
