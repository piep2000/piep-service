const config = require('./jest.config');
config.collectCoverage = false;
config.testMatch = [
    '**/test/integration/**/*.test.[jt]s?(x)'
];
module.exports = config;
