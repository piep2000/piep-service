import {ResourceConflictError, Utils} from '@mbl/base';
import {Injector}                     from '@mbl/di';
import {Logger}                       from '@mbl/logger';
import {MessageController}            from '../../../../src/controllers/ExampleController';
import {Example}                      from '../../../../src/models/Example';
import {settings}                     from '../../../../src/settings/Settings';

describe('ExampleController', () => {
    const _id: string = '5c8f5751bcec4446b7324e29';
    const id: number = 3;
    const somethingWentWrong = 'Something went wrong!';

    const createReqMock = (name: string, data: any) => {
        const r = {
            originalUrl: settings.http.httpPrefix
        };
        // @ts-ignore
        r[name] = data;
        return r;
    };
    let returnData: any = {};
    let statusSend: number;
    let resMock: any;

    describe('.list()', () => {
        beforeEach(() => {
            returnData = null;
            statusSend = 0;
            resMock = {
                json: jest.fn().mockImplementation(data => returnData = data),
                sendStatus: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                }),
                status: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                })
            };
        });

        it('should return a list of Examples', async () => {
            const mockData = [{_id, id}];
            const spy = jest.spyOn(Example, 'getList').mockResolvedValueOnce({count: 1, data: mockData});

            const reqMock: any = createReqMock('query', {page: 1, perPage: 5});

            const exampleController = Injector.get(MessageController);
            await exampleController.list(reqMock, resMock);

            expect(returnData).toEqual(mockData);

            spy.mockRestore();
        });
    });

    describe('.get()', () => {
        beforeEach(() => {
            returnData = null;
            statusSend = 0;
            resMock = {
                json: jest.fn().mockImplementation(data => returnData = data),
                sendStatus: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                }),
                status: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                })
            };
        });

        it('should successfully return data', async () => {
            const spy = jest.spyOn(Example, 'get').mockResolvedValueOnce({_id, id});
            const reqMock: any = createReqMock('params', {id});

            const exampleController = Injector.get(MessageController);
            await exampleController.get(reqMock, resMock);

            expect(returnData).toEqual({_id, id});

            spy.mockRestore();
        });

        it('should return 404 Not Found', async () => {
            const id: number = 5634533;

            const spy = jest.spyOn(Example, 'get').mockResolvedValueOnce(null);
            const reqMock: any = createReqMock('params', {id});
            const exampleController = Injector.get(MessageController);
            await exampleController.get(reqMock, resMock);

            expect(statusSend).toEqual(404);
            expect(returnData).toEqual(null);

            spy.mockRestore();
        });

        it('should handle Error correctly', async () => {
            const invalidId: string = '5c8f5751bcec4446b7e29';

            let output: string = '';
            const logSpy = jest.spyOn(Logger.prototype, 'error').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });

            const exampleSpy = jest.spyOn(Example, 'get').mockImplementationOnce(async () => {
                throw new Error(somethingWentWrong);
            });

            const reqMock: any = createReqMock('params', {id: invalidId});
            const exampleController = Injector.get(MessageController);
            await exampleController.get(reqMock, resMock);

            expect(resMock.json).toHaveBeenCalledTimes(0);
            expect(returnData).toEqual(null);
            expect(output).toContain(somethingWentWrong);
            expect(statusSend).toEqual(500);

            exampleSpy.mockRestore();
            logSpy.mockRestore();
        });
    });

    describe('.create()', () => {
        beforeEach(() => {
            returnData = null;
            statusSend = 0;
            resMock = {
                header: (header: string, value: string) => resMock.headers[header] = value,
                headers: {},
                json: jest.fn().mockImplementation(data => returnData = data),
                sendStatus: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                }),
                status: jest.fn().mockImplementation(status => {
                    statusSend = status;
                    return resMock;
                })
            };
        });

        it('should successfully create a new resource', async () => {
            const spy = jest.spyOn(Example, 'create').mockResolvedValueOnce({_id, id});
            const reqMock: any = createReqMock('body', {id});

            const exampleController = Injector.get(MessageController);
            await exampleController.create(reqMock, resMock);

            expect(statusSend).toEqual(201);
            expect(returnData).toEqual({_id, id});

            spy.mockRestore();
        });

        it('should fail with status 409 when trying to insert an existing document', async () => {
            const spy = jest.spyOn(Example, 'create').mockRejectedValue(new ResourceConflictError({resourceId: id}));
            const reqMock: any = createReqMock('body', {id});

            const exampleController = Injector.get(MessageController);
            await exampleController.create(reqMock, resMock);

            expect(statusSend).toEqual(409);
            expect(resMock.headers.Location).toEqual(Utils.joinUri(`${reqMock.originalUrl}/${id}`));
            expect(returnData).toEqual(null);

            spy.mockRestore();
        });

        it('should fail with status 500 when an unexpected error occurs', async () => {
            let output: string = '';
            const logSpy = jest.spyOn(Logger.prototype, 'error').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });

            const exampleSpy = jest.spyOn(Example, 'create').mockRejectedValue(new Error(somethingWentWrong));

            const reqMock: any = createReqMock('body', {id});
            const exampleController = Injector.get(MessageController);
            await exampleController.create(reqMock, resMock);

            expect(resMock.json).toHaveBeenCalledTimes(0);
            expect(returnData).toEqual(null);
            expect(output).toContain(somethingWentWrong);
            expect(statusSend).toEqual(500);

            exampleSpy.mockRestore();
            logSpy.mockRestore();
        });
    });
});
