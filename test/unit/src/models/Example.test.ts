import {ResourceConflictError} from '@mbl/base';
import {Injector}              from '@mbl/di';
import {Mongo}                 from '@mbl/mongo';
import {ObjectId}              from 'bson';
import {merge}                 from 'lodash';
import {MongoMemoryServer}     from 'mongodb-memory-server';
import {Example}               from '../../../../src/models/Example';
import {settings}              from '../../../../src/settings/Settings';
import MongoMobilea            from '../../../../src/storage/MongoMobilea';

// May require additional time for downloading MongoDB binaries
jest.setTimeout(60000);

let mongoMemoryServer: MongoMemoryServer;
let connectUri: string;
let mongo: Mongo;
const id: number = 1;
const nonExistingId: number = 2;
const _nonExistingId: string = 'FFFFFFFFFFFFFFFFFFFFFFFF';
let createdId: string;

describe('Example', () => {
    beforeAll(async () => {
        mongoMemoryServer = new MongoMemoryServer();
        connectUri = await mongoMemoryServer.getConnectionString();
        jest.setTimeout(5000); // When connected, the timeout can be restored to it's default.
    });

    beforeEach(async () => {
        mongo = Injector.get(MongoMobilea);
        mongo.configure(merge({}, settings.mongo, {connectUri}));
        await mongo.ensureConnection();
    });

    afterEach(async () => {
        await mongo.close();
    });

    afterAll(async () => {
        await mongoMemoryServer.stop();
    });

    describe('::create()', () => {
        it('should insert a new Example and return the data', async () => {
            const example = await Example.create({id});
            expect(example).toBeInstanceOf(Example);
            expect(example._id).toBeInstanceOf(ObjectId);
            expect(example._id.toString()).toMatch(/^[a-fA-F\d]{24}$/); //  must be a string of 24 hex characters
            createdId = example._id.toString();
        });

        it('should fail inserting a duplicate Example document', async done => {
            try {
                await Example.create({id});
            } catch (err) {
                expect(err).toBeInstanceOf(ResourceConflictError);
                expect(err.resourceId).toBeInstanceOf(ObjectId);
                expect(err.resourceId.toString()).toMatch(createdId);
                done();
            }
        });
    });

    describe('::get()', () => {
        it('should get an existing document by id', async () => {
            const example = await Example.get({id: id.toString()});
            expect(example).toBeInstanceOf(Example);
            expect(example._id).toBeInstanceOf(ObjectId);
            expect(example._id.toString()).toMatch(createdId);
        });
        it('should get an existing document by _id', async () => {
            const example = await Example.get({id: createdId});
            expect(example).toBeInstanceOf(Example);
            expect(example.id).toEqual(id);
        });
        it('should return null when document is not found by id', async () => {
            const example = await Example.get({id: nonExistingId.toString()});
            expect(example).toEqual(null);
        });
        it('should return null when document is not found by _id', async () => {
            const example = await Example.get({id: _nonExistingId});
            expect(example).toEqual(null);
        });
        it('should throw an Error when an invalid search param was provided', async done => {
            const invalidSearchParam = {
                invalidSearchParam: 'invalidSearchParam'
            };
            try {
                await Example.get(<any>invalidSearchParam);
            } catch (err) {
                expect(err.message).toContain('No valid search param was provided!');
                done();
            }
        });
    });

    describe('::getList()', () => {
        it('should get an list of documents', async () => {
            const {data} = await Example.getList({}, {page: 1, perPage: 5});
            expect(data).toBeInstanceOf(Array);
            expect(data.length).toBeLessThanOrEqual(5);
            expect(data[0]._id).toBeInstanceOf(ObjectId);
            expect(data[0]._id.toString()).toMatch(createdId);
        });
    });

    describe('::getIdFilter()', () => {
        it('should return `filter.id` as a number', async () => {
            const filter = (<any>Example).getIdFilter({id: '1'});
            expect(filter.hasOwnProperty('id')).toBe(true);
            expect(filter.id).toBe(1);
        });
        it('should return `filter._id` as a ObjectID', async () => {
            const filter = (<any>Example).getIdFilter({id: '5ca1e718e9c3b044d98e5676'});
            expect(filter.hasOwnProperty('_id')).toBe(true);
            expect(filter._id).toBeInstanceOf(ObjectId);
        });
    });
});
