import {Injector}          from '@mbl/di';
import {Http}              from '@mbl/http';
import {Logger}            from '@mbl/logger';
import {MessageController} from '../../src/controllers/ExampleController';
import {MicroService}      from '../../src/MicroService';
import {settings}          from '../../src/settings/Settings';
import MongoMobilea        from '../../src/storage/MongoMobilea';

describe('MicroService', () => {
    const checkCalled = () => jest.fn().mockImplementationOnce(() => true);
    const throwError = () => jest.fn().mockImplementationOnce(async () => {
        throw new Error('');
    });

    describe('.httpConfigure()', () => {
        it('should configure the Http module', () => {
            let output: string = '';
            const spy = jest.spyOn(Logger.prototype, 'info').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });
            const http = Injector.get(Http);

            expect(settings.http.host).toEqual((<any>http).settings.host);
            expect(output).toContain('Configuring HTTP');
            spy.mockRestore();
        });
    });

    describe('.mongoConfigure()', () => {
        it('should configure the MongoMobilea module', () => {
            let output: string = '';
            const spy = jest.spyOn(Logger.prototype, 'info').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });
            const mongo = Injector.get(MongoMobilea);

            expect(settings.mongo.database).toEqual((<any>mongo).settings.database);
            expect(output).toContain('Configuring MongoDB');
            spy.mockRestore();
        });
    });

    describe('.exampleControllerConfigure()', () => {
        it('should configure the ExampleController', () => {
            let output: string = '';
            const spy = jest.spyOn(Logger.prototype, 'info').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });
            const exampleController = Injector.get(MessageController);

            expect(settings).toEqual((<any>exampleController).settings);
            expect(output).toContain('Configuring exampleController');
            spy.mockRestore();
        });
    });

    describe('::run()', () => {
        let spy: jest.SpyInstance;
        beforeEach(() => {
            spy = jest.spyOn(MicroService.prototype, 'start').mockImplementation(async () => {
                //
            });
        });

        afterEach(() => {
            spy.mockRestore();
        });

        it('should call the MicroService.start function', async () => {
            await MicroService.run();
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('.start()', () => {
        let microService: MicroService;
        beforeEach(() => {
            MicroService.configure = checkCalled();
            MongoMobilea.prototype.ensureConnection = checkCalled();
            Http.prototype.listen = checkCalled();
            microService = new MicroService();
        });

        it('should call MicroService.configure', async () => {
            await microService.start();
            expect(MicroService.configure).toHaveBeenCalledTimes(1);
        });
        it('should call MongoMobilea.isAvailable function', async () => {
            await microService.start();
            expect(MongoMobilea.prototype.ensureConnection).toHaveBeenCalledTimes(1);
        });

        it('should fail to call MongoMobilea.isAvailable function', async done => {
            MongoMobilea.prototype.ensureConnection = throwError();
            try {
                await microService.start();
            } catch (e) {
                expect(Http.prototype.listen).toHaveBeenCalledTimes(0);
                expect(MongoMobilea.prototype.ensureConnection).toHaveBeenCalledTimes(1);
                done();
            }
        });

        it('should call Http.listen function', async () => {
            await microService.start();
            expect(Http.prototype.listen).toHaveBeenCalledTimes(1);
        });
    });

    describe('.stop()', () => {
        it('should stop the MicroService', async () => {
            let output: string = '';
            const spyLogger = jest.spyOn(Logger.prototype, 'info').mockImplementationOnce((...args: any[]) => {
                output += args.join(' ');
                return this;
            });
            const spyHttp = jest.spyOn(Http.prototype, 'close').mockResolvedValueOnce(undefined);
            // @ts-ignore
            const spyExit = jest.spyOn(process, 'exit').mockImplementationOnce((): never => {
                // empty body, we don't really want to exit here
            });
            const microService: MicroService = new MicroService();
            await microService.stop();

            expect(output).toContain('Application stopped');
            expect(spyHttp).toHaveBeenCalledTimes(1);
            expect(spyExit).toHaveBeenCalledTimes(1);

            spyExit.mockRestore();
            spyHttp.mockRestore();
            spyLogger.mockRestore();
        });
    });
});
