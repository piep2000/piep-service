const config = require('./jest.config');
config.testMatch = [
    '**/test/unit/**/*.test.[jt]s?(x)'
];
module.exports = config;
