/**
 * Run some code here to configure or setup the testing environment.
 * This will run before each test.
 */
import {Logger}   from '@mbl/logger';
import {settings} from '../src/settings/Settings';

// Overwrite some logging settings
settings.logging.debug = false;
settings.logging.error = false;
settings.logging.info = false;
settings.logging.tryParseSourceMaps = false;
settings.logging.warn = false;
// Overwrite http setting so it doesn't actually starts listening.
settings.http.port = -1;

Logger.configure(settings.logging);
