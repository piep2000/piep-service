/**
 * Teardown your global dependencies, stop long lived services here.
 * Keep in mind, this will be run once after all test suites. But wont be in the same context as them.
 * You have access to the `globals` variable here
 */
module.exports = async () => {
    // Comment to prevent "block is empty" error
};
