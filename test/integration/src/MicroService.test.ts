import {Utils}             from '@mbl/base';
import {Injector}          from '@mbl/di';
import {Http}              from '@mbl/http';
import {MongoMemoryServer} from 'mongodb-memory-server';
import * as path           from 'path';
import * as request        from 'supertest';
import {MicroService}      from '../../../src/MicroService';
import {settings}          from '../../../src/settings/Settings';

// May require additional time for downloading MongoDB binaries
jest.setTimeout(60000);

let mongoMemoryServer: MongoMemoryServer;
let http: Http;
const id: number = 1;
let location: string;
let mockCwd: jest.SpyInstance;

const testRoot = path.resolve(__dirname, '../../../');

describe('MicroService integration', () => {
    beforeAll(async () => {
        mockCwd = jest.spyOn(process, 'cwd').mockReturnValue(testRoot);
        mongoMemoryServer = new MongoMemoryServer();
        settings.mongo.connectUri = await mongoMemoryServer.getConnectionString();
        jest.setTimeout(5000); // When connected, the timeout can be restored to it's default.
        await MicroService.run();
        http = Injector.get(Http);
    });

    afterAll(async () => {
        const microService = Injector.getRootApplication<MicroService>();
        microService.stop().catch(); // We do not want to await the stop because it will kill JEST as well...
        await mongoMemoryServer.stop();
        mockCwd.mockRestore();
    });

    describe('ready', () => {

        it('should create a new Example resource and return it', async () => {
            const response = await request(http.express).post(`${settings.http.httpPrefix}`).set('Accept', 'application/json').send({id});
            expect(response.status).toBe(201);
            expect(response.header.hasOwnProperty('content-type')).toBe(true);
            expect(response.header['content-type']).toContain('application/json');
            expect(response.body._id).toMatch(/^[a-fA-F\d]{24}$/); //  must be a string of 24 hex characters
        });

        it('should not be able recreate an existing Example resource', async () => {
            const response = await request(http.express).post(`${settings.http.httpPrefix}`).set('Accept', 'application/json').send({id});
            expect(response.status).toBe(409);
            expect(response.header.hasOwnProperty('location')).toBe(true);
            location = response.header.location;
        });

        it('should be able to fetch an Example resource by received location header', async () => {
            const response = await request(http.express).get(location);
            expect(response.status).toBe(200);
            expect(response.body.hasOwnProperty('id')).toBe(true);
            expect(response.body.id).toBe(id);
        });

        it('should be able to fetch an Example resource by numeric id', async () => {
            const response = await request(http.express).get(`${Utils.joinUri(settings.http.httpPrefix, id)}`);
            expect(response.status).toBe(200);
            expect(response.body.hasOwnProperty('id')).toBe(true);
            expect(response.body.id).toBe(id);
        });

        it('should return a 404 status when resource could not be found', async () => {
            const nonExistingId: number = 2;
            const response = await request(http.express).get(`${Utils.joinUri(settings.http.httpPrefix, nonExistingId)}`);
            expect(response.status).toBe(404);
        });

        it('should return an 400 status when getting a resource with an invalid id', async () => {
            const invalidId: string = '32rfks634s';
            const response = await request(http.express).get(`${Utils.joinUri(settings.http.httpPrefix, invalidId)}`);
            expect(response.status).toBe(400);
        });
    });
});
