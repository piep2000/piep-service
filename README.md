[![pipeline status](https://gitlab.com/piep2000/piep-service/badges/master/pipeline.svg)](https://gitlab.com/piep2000/piep-service/pipelines)
[![coverage report](https://gitlab.com/piep2000/piep-service/badges/master/coverage.svg)](https://gitlab.com/piep2000/piep-service)

# piep-service
Service for PIEP2000

---

## Lint your code
If you are using WebStorm, go to Settings, search for `tslint` and enable it. And make sure it will look for `tslint.json`.

Or run this in your terminal, if you want to do it manually: 
```bash
yarn run lint
```

## Run the tests
All the tests:

```bash
yarn run test
```

or in docker:
```bash
docker exec -it $(docker container ls --format "{{.Names}}" | grep piep_service | head -1) yarn run test
```


Run the unit tests:
```bash
yarn run test:unit
```

or in docker:
```bash
docker exec -it $(docker container ls --format "{{.Names}}" | grep piep_service | head -1) yarn run test:unit
```

Run the integration tests:
```bash
yarn run test:integration
```

or in docker:
```bash
docker exec -it $(docker container ls --format "{{.Names}}" | grep piep_service | head -1) yarn run test:integration
```

## Building the code
```bash
yarn run build
```

## Deploying the development environment
Before anything we need the default docker-local to mount our files into the container, run:
```bash
cp config/docker-local.yml.example config/docker-local.yml
```

After that we can deploy it:
```bash
./deploy.sh
```

If you want some verbose info, run:
```bash
./deploy.sh true
```

Afterwards the service will be available at `https://<domain>.dev.mobilea.nl:8443/piep`

If you want to take a look at the traefik dashboard, visit `https://<domain>.dev.mobilea.nl:8443/traefik/piep`

[comment]: <> (begin environment)
## Configurable Environment variables

| Variable name                                               | Type          | Default value                                                                                                    
| :---------------------------------------------------------- | :------------ | :---------------------------------------------------------------------------------------------------------------- |
| process.env.logging_debug                                   | boolean       | false                                                                                                             |
| process.env.logging_error                                   | boolean       | true                                                                                                              |
| process.env.logging_info                                    | boolean       | true                                                                                                              |
| process.env.logging_tryParseSourceMaps                      | boolean       | true                                                                                                              |
| process.env.logging_warn                                    | boolean       | true                                                                                                              |
| process.env.http_connectionClose                            | boolean       | true                                                                                                              |
| process.env.http_headers_accessControlAllowHeaders          | string        | "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Real-IP, X-Forwarded-For, X-Protocol-Version"   |
| process.env.http_headers_accessControlAllowMethods          | string        | "DELETE,GET,OPTIONS,PATCH,POST,PUT"                                                                               |
| process.env.http_headers_accessControlAllowOrigin           | string        | "*"                                                                                                               |
| process.env.http_headers_vary                               | string        | "Accept-Encoding"                                                                                                 |
| process.env.http_host                                       | string        | "0.0.0.0"                                                                                                         |
| process.env.http_httpPrefix                                 | string        | "/"                                                                                                               |
| process.env.http_incomingRequestLogger                      | string        | "info"                                                                                                            |
| process.env.http_port                                       | number        | 3000                                                                                                              |
| process.env.http_set_xPoweredBy                             | boolean       | false                                                                                                             |
| process.env.http_set_trustProxy                             | boolean       | true                                                                                                              |
| process.env.http_set_connection                             | string        | "Close"                                                                                                           |
| process.env.http_exposeValidationErrorsInResponse           | boolean       | true                                                                                                              |
| process.env.http_maxBodyLength                              | string        | "50mb"                                                                                                            |
| process.env.mongo_connectUri                                | string        | ""                                                                                                                |
| process.env.mongo_database                                  | string        | ""                                                                                                                |
| process.env.mongo_connectOptions_minSize                    | number        | 1                                                                                                                 |
| process.env.mongo_connectOptions_poolSize                   | number        | 10                                                                                                                |
| process.env.mongo_connectOptions_reconnectTries             | number        | 60                                                                                                                |
| process.env.rss_url                                         | string        | ""                                                                                                                |
| process.env.fireStore_projectId                             | string        | ""                                                                                                                |
| process.env.fireStore_credentials_type                      | string        | ""                                                                                                                |
| process.env.fireStore_credentials_projectId                 | string        | ""                                                                                                                |
| process.env.fireStore_credentials_privateKeyId              | string        | ""                                                                                                                |
| process.env.fireStore_credentials_privateKey                | string        | ""                                                                                                                |
| process.env.fireStore_credentials_clientEmail               | string        | ""                                                                                                                |
| process.env.fireStore_credentials_clientId                  | string        | ""                                                                                                                |
| process.env.fireStore_credentials_authUri                   | string        | ""                                                                                                                |
| process.env.fireStore_credentials_tokenUri                  | string        | ""                                                                                                                |
| process.env.fireStore_credentials_authProviderX509CertUrl   | string        | ""                                                                                                                |
| process.env.fireStore_credentials_clientX509CertUrl         | string        | ""                                                                                                                |
| process.env.sentry_dsn                                      | string        | ""                                                                                                                |
| process.env.sentry_release                                  | string        | ""                                                                                                                |

[comment]: <> (end environment)
