#!/usr/bin/env bash
LOG_INFO=0
LOG_ERROR=1;
LOG_WARN=2;
LOG_DEBUG=99;

function log() {
    if [[ "${1}" =~ \[[0-9]{4}-[0-9]{2}-[0-9]{2}\ [0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}\] ]]; then
        echo "${1}"
    else
        echo "$(date +"[%Y-%m-%d %T.%3N]") ${1}"
    fi
}

function empty() {
    # Only echo matched lines and ignore all others, it's a bit like 1>/dev/null but with matching.
    while read line
    do
        if [ ! -z "${line}" ]; then
            if [[ "${line}" =~ \[[0-9]{4}-[0-9]{2}-[0-9]{2}\ [0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}\] ]]; then
                printf "${line}\n"
            fi
        fi
    done
}

function info() {
    if [ ${LOG_LEVEL} -ge ${LOG_INFO} ]; then
        if [ -z "${1}" ]; then
            while read line
            do
                if [ ! -z "${line}" ]; then
                    printf "$(log "${line}")\n"
                fi
            done
        else
            printf "$(log "${1}")\n"
        fi
    fi
}

function error() {
    if [ ${LOG_LEVEL} -ge ${LOG_ERROR} ]; then
        if [ -z "${1}" ]; then
            while read line
            do
                if [ ! -z "${line}" ]; then
                    printf "\033[1;31m$(log "${line}")\033[0m\n"
                fi
            done
        else
            printf "\033[1;31m$(log "${1}")\033[0m\n"
        fi
    fi
}

function warn() {
    if [ ${LOG_LEVEL} -ge ${LOG_WARN} ]; then
        if [ -z "$1" ]; then
            while read line
            do
                if [ ! -z "${line}" ]; then
                    printf "\033[1;33m$(log "${line}")\033[0m\n"
                fi
            done
        else
            printf "\033[1;33m$(log "${1}")\033[0m\n"
        fi
    fi
}

function debug() {
    if [ ${LOG_LEVEL} -ge ${LOG_DEBUG} ]; then
        if [ -z "$1" ]; then
            while read  line
            do
                if [ ! "$line" ]; then
                    printf "\033[1;36m$(log "${line}")\033[0m\n"
                fi
            done
        else
            printf "\033[1;36m$(log "${1}")\033[0m\n"
        fi
    fi
}

function to_slug() {
    echo $1 | awk '{print tolower($0)}' | perl -pe 's/[^a-z0-9\n]/-/'
}

function pull_image() {
    debug "Pulling image \"$1\""
    docker pull $1 > >(empty) 2> >(error)
    if [ $? -eq 1 ]; then
        exit 1
    fi
}

function export_env() {
    # Usage: export_env <name> <value> [hide: boolean]
    # Example:
    #   export_env "HTTP_PORT" 3000
    #   export_env "SOME_PASSWORD" "someValue" true

    if [ -z "$3" ]; then
        debug "Exporting $1=\"$2\""
    else
        debug "Exporting $1=\"$(printf "%${#2}s" | tr " " "*")\""
    fi

    printf -v $1 $2
    export $1
}

LOG_LEVEL=$1
case ${LOG_LEVEL} in
    ''|*[!0-9]*) LOG_LEVEL=2 ;;
esac

LOG_LEVELS=()
[[ ${LOG_LEVEL} -gt ${LOG_INFO} ]] && LOG_LEVELS+=("info")
[[ ${LOG_LEVEL} -gt ${LOG_ERROR} ]] && LOG_LEVELS+=("error")
[[ ${LOG_LEVEL} -gt ${LOG_WARN} ]] && LOG_LEVELS+=("warn")
[[ ${LOG_LEVEL} -gt ${LOG_DEBUG} ]] && LOG_LEVELS+=("debug")
debug "Logging the following levels: $(echo "${LOG_LEVELS[*]}" | sed 's/ /, /g')"

if [ ! -f ./config/local.json ]; then
    warn "Missing 'local.json', creating empty one"
    echo "{}" > ./config/local.json
fi

if [ -z "$CI" ]; then
    export_env "CI_PROJECT_PATH" `git remote get-url origin | perl -pe 's/^(?:git@|https:\/\/){1}(.*?)(?::|\/){1}(.*)\.git$/\2/g'`
    export_env "CI_PROJECT_NAME" `echo ${CI_PROJECT_PATH} | perl -wnE 'say /([^\/]+(?=\/$|$))/'`
    export_env "CI_PROJECT_NAMESPACE" `echo ${CI_PROJECT_PATH} | perl -pe 's/[^\/]+(?=\/$|$)//g'`

    export_env "CI_REGISTRY" `git remote get-url origin | perl -pe 's/^(?:git@|https:\/\/){1}(.*?)(?::|\/){1}(.*)\.git$/registry.\1/g'`
    export_env "CI_REGISTRY_IMAGE" "${CI_REGISTRY}/${CI_PROJECT_PATH}"

    export_env "CI_COMMIT_SHA" `git rev-parse HEAD`
    export_env "CI_COMMIT_SHORT_SHA" `echo ${CI_COMMIT_SHA} | cut -c1-8`
    export_env "CI_COMMIT_REF_NAME" `git branch | grep \* | cut -d ' ' -f2`
    export_env "CI_COMMIT_REF_SLUG" `to_slug ${CI_COMMIT_REF_NAME} | cut -c1-63`
fi

if [ $(echo "${CI_COMMIT_REF_NAME}" | grep -Eq  ^v[0-9]+\.[0-9]+\.[0-9]+$) ]; then
    info "Running on a tagged release ($CI_COMMIT_REF_NAME) [$CI_COMMIT_SHORT_SHA]"
    export_env "CI_APPLICATION_REPOSITORY" "${CI_REGISTRY_IMAGE}/${CI_PROJECT_NAME}"
    export_env "CI_APPLICATION_TAG" "${CI_COMMIT_REF_NAME}"
else
    info "Running on a feature branch ($CI_COMMIT_REF_NAME) [$CI_COMMIT_SHORT_SHA]"
    export_env "CI_APPLICATION_REPOSITORY" "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}"
    export_env "CI_APPLICATION_TAG" "${CI_COMMIT_SHA}"
fi

if [[ -z "${CI}" ]]; then
    export_env "LB_HOSTS" "HostRegexp:{subdomain:[a-z]+}.dev.mobilea.nl"
fi
if [[ -z "${LB_HOSTS}" ]]; then
    export_env "LB_HOSTS" "Host:localhost"
fi

# Setting up all the needed variables for this service.
debug "Creating missing variables, if any"
if [[ -z "$CI" ]] || [[ -z "${http_port}" ]]; then
    export_env "http_port" `jq -r -s '.[0] * .[1] | .http.port' ./config/environment.json ./config/local.json`
fi

if [ -z "$CI" ]; then
    info "Preparing development area"
    export_env "SERVICE_NAME" `echo ${CI_PROJECT_NAME} | perl -pe 's/(-[A-Za-z]+)/ /g'`
    if [[ -d .githooks && `git config core.hooksPath` != ".githooks" ]]; then
        debug "Setting git hooks path to '.githooks'"
        git config core.hooksPath .githooks > >(info) 2> >(error)
    fi
    sudo rm -rf node_modules/ dist/
    mkdir node_modules/
    export_env "PWD" `pwd`
fi

STACK_CONFIGS="-c config/docker-compose.yml"
export_env "SERVICE_IMAGE" "${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}"
if [ -f ./config/docker-local.yml ]; then
    debug "'docker-local.yml' found, overwriting the SERVICE_IMAGE with the dev variant."
    export_env "SERVICE_IMAGE" "${SERVICE_IMAGE}-dev"
    STACK_CONFIGS="${STACK_CONFIGS} -c config/docker-local.yml"
fi
pull_image ${SERVICE_IMAGE}

info "Deploying '${SERVICE_NAME}' stack"
docker stack deploy ${STACK_CONFIGS} ${SERVICE_NAME} > >(info) 2> >(error)
sleep 2
