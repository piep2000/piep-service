import {Logger}                 from '@mbl/logger';
import {captureException, init} from '@sentry/node';
import {Error}                  from 'tslint/lib/error';
import {name,version}           from './package.json';
import {MicroService}           from './src/MicroService';
import {settings}               from './src/settings/Settings';

settings.sentry.release = settings.sentry.release || `${name}@${version}`;

for (const key in settings.sentry) {
    if ((<any>settings.sentry)[key] === '') {
        throw new Error(`Missing '${key}' in settings.sentry`);
    }
}
init(settings.sentry);

/**
 * Call the `MicroService.run` function to get things started
 */
MicroService.run()
    .catch(err => {
        Logger.create('MicroService').error(err);
        captureException(err);

        setTimeout(() => process.exit(1), 5000);
    });
