# syntax=docker/dockerfile:experimental
FROM node:10.15.3-alpine AS build

WORKDIR /usr/src/app

# Global dependencies that we always need.
RUN apk add --update --no-cache perl

# Creating default rebuild-deps script (don't forget to remove # when using).
RUN echo "" > /usr/local/bin/rebuild-deps && chmod +x /usr/local/bin/rebuild-deps

# Creating install-deps script.
RUN echo -e  "apk --no-cache add --virtual native-deps g++ gcc libgcc libstdc++ linux-headers make python git jq openssh-client\n" \
            # Setting up SSH.
            "mkdir -p -m 0600 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts\n" \
            "if [ ! -z \"\$PRV_SSH_KEY\" ]; then\n" \
            "    eval \`ssh-agent -s\`\n" \
            "    echo \"\$PRV_SSH_KEY\" | ssh-add -\n" \
            "fi\n" \
            # Removing any modules if exists.
            " rm -rf node_modules" \
            # Installing dependencies.
            " && yarn install \$YARN_ARGS" \
            # Call the rebuild-deps script.
            " && rebuild-deps" \
            # Removing unused data.
            " && rm -rf /usr/local/share/.cache/yarn" \
            " && rm -rf ~/.ssh" \
            " && apk del native-deps" > /usr/local/bin/install-deps && chmod +x /usr/local/bin/install-deps

COPY [ "./config/environment.json", "./config/" ]
COPY [ "./src", "./src/" ]
COPY [ "./package.json", "./yarn.lock", "./tsconfig.json", "index.ts", "./" ]

RUN --mount=type=ssh install-deps \
    && yarn run build \
    && rm -rf ./node_modules/ \
    && rm -rf ./*.ts ./src/ \
    && mv ./dist/* ./ \
    && rm -rf ./dist/ ./config/ ./tsconfig.json

# ------------------------------------------ #
#              Production stage              #
# ------------------------------------------ #
FROM build AS prod

ENV TZ="Europe/Amsterdam"
ENV NODE_ENV="production"
ENV YARN_ARGS="--production"

# Global dependencies that we always need.
RUN apk add --update --no-cache tzdata

# Adding to rebuild-deps script (don't forget to remove # when using).
RUN echo "# yarn add bcrypt@\`jq -r '.dependencies.bcrypt' < package.json\` --exact" >> /usr/local/bin/rebuild-deps

RUN --mount=type=ssh install-deps

CMD [ "node", "index.js" ]

# ------------------------------------------ #
#             Development stage              #
# ------------------------------------------ #
FROM prod AS dev

ENV NODE_ENV="development"
ENV YARN_ARGS=""

COPY [ "./config/environment.json", "./config/" ]
COPY [ "./src", "./src/" ]
COPY [ "./index.ts", "./tsconfig.json", "./" ]
RUN rm -rf ./src/*.js ./index.js

# Adding to rebuild-deps script (don't forget to remove # when using).
RUN echo "# yarn add bcrypt@`jq -r '.dependencies.bcrypt' < package.json` --exact" >> /usr/local/bin/rebuild-deps

RUN --mount=type=ssh install-deps

CMD [ "yarn", "run", "watch" ]
